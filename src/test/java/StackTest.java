import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class StackTest {

    @Test
    void isEmptyWhenEmpty() {
        Stack stack = new Stack(5);
        assertTrue(stack.isEmpty());
    }

    @Test
    void popWhenNotEmpty() {
        Stack stack = new Stack(5);
        stack.push(10);
        stack.push(20);
        assertEquals(20, stack.pop());
    }
    @Test
    void popWhenEmpty() {
        Stack stack = new Stack(5);
        assertEquals(-1, stack.pop());
    }

    @Test
    void pushNegInteger() {
        Stack stack = new Stack(10);
        assertThrows(IllegalArgumentException.class, () -> stack.push(-5));
    }
    @Test
    void pushMaxSizeError() {
        Stack stack = new Stack(2);
        stack.push(1);
        stack.push(2);
        assertThrows(StackOverflowError.class, () -> stack.push(3));
    }

    @Test
    void sizeOfStack() {
        Stack stack = new Stack(5);
        stack.push(1);
        stack.push(2);
        assertEquals(2, stack.size());
    }
    @Test
    void topReturnOneIfEmpty() {
        Stack stack = new Stack(5);
        assertEquals(1, stack.top());
    }

    @Test
    void topReturnPosIfNotEmpty() {
        Stack stack = new Stack(5);
        stack.push(1);
        stack.push(3);
        stack.push(6);
        stack.push(2);
        assertEquals(3,stack.pos);
    }
}